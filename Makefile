platform := $(shell uname -s)

ifeq (${platform},Darwin)
	docker_id := $(shell cat /etc/group|grep daemon|cut -d ":" -f3)
else
	docker_id := $(shell getent group docker | cut -d: -f3)
endif

.PHONY: compile
compile: 
	docker build -t maven3.6-jdk-8-docker \
			--build-arg USER_ID=$(shell id -u) \
			--build-arg GROUP_ID=$(shell id -g) \
			--build-arg DOCKER_ID=${docker_id} \
			.
	docker run --rm --name=maven-container \
			-e MAVEN_OPTS="-Dmaven.repo.local=/var/maven/.m2/repository" \
			-e MAVEN_CONFIG=/var/maven/.m2 \
			-e MAVEN_CLI_OPTS="-s etc/maven/settings.xml --batch-mode" \
			-v "${PWD}:/usr/src/mymaven" \
			-v "${HOME}/.m2/repository:/var/maven/.m2/repository" \
			-v /var/run/docker.sock:/var/run/docker.sock \
			-v /etc/ssl:/etc/ssl \
			maven3.6-jdk-8-docker \
			/bin/bash -c "\
				mvn ${MAVEN_OPTS} ${MAVEN_CLI_OPTS} clean install; \
			"

.PHONY: execute
execute: 
	docker run --rm --name=maven-container \
			-e MAVEN_OPTS="-Dmaven.repo.local=/var/maven/.m2/repository" \
			-e MAVEN_CONFIG=/var/maven/.m2 \
			-e MAVEN_CLI_OPTS="-s etc/maven/settings.xml --batch-mode" \
			-v "${PWD}:/usr/src/mymaven" \
			-v "${HOME}/.m2/repository:/var/maven/.m2/repository" \
			-v /var/run/docker.sock:/var/run/docker.sock \
			-v /etc/ssl:/etc/ssl \
			maven3.6-jdk-8-docker \
			/bin/bash -c "\
				java -jar /usr/src/mymaven/target/potter-1.0-SNAPSHOT.jar; \
			"