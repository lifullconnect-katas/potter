FROM maven:3.6-jdk-8

ARG USER_ID
ARG GROUP_ID
ARG DOCKER_ID

RUN grep -i $GROUP_ID /etc/group || addgroup --gid $GROUP_ID user && \
    adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user

RUN grep -i ":$DOCKER_ID:" /etc/group || addgroup --gid $DOCKER_ID docker;

RUN apt-get update && \
    apt-get install -y --no-install-recommends docker.io && \
    rm -rf /var/lib/apt/lists/*;

RUN usermod -aG docker user; \
    touch /var/run/docker.sock; \
    chgrp docker /var/run/docker.sock


USER user

VOLUME /usr/src/mymaven

WORKDIR /usr/src/mymaven
