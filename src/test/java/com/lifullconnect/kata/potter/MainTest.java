package com.lifullconnect.kata.potter;

import org.junit.Test;
import org.junit.Assert;

public class MainTest {

    @Test
    public void getText_shouldReturnHelloWorld() {
        Assert.assertEquals(Main.getText(), "Hello World!");
    }

}